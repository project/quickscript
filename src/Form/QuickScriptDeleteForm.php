<?php

/**
 * @file
 * Contains \Drupal\quickscript\Form\QuickScriptDeleteForm.
 */

namespace Drupal\quickscript\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Quick Script entities.
 *
 * @ingroup quickscript
 */
class QuickScriptDeleteForm extends ContentEntityDeleteForm {

}
